# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit config multilib

DESCRIPTION="Manage environment variables set in /etc/env.d/"
SVN_DATE='$Date: 2007-11-30 12:28:37 -0500 (Fri, 30 Nov 2007) $'
VERSION=$(svn_date_to_version "${SVN_DATE}" )

# Classes of env-vars
SPACE_CLASS=( CONFIG_PROTECT
    CONFIG_PROTECT_MASK )
PATH_CLASS=( ADA_INCLUDE_PATH
    ADA_OBJECT_PATH
    CLASSPATH
    INFODIR
    INFOPATH
    KDEDIRS
    MANPATH
    PATH
    PKG_CONFIG_PATH
    PRELINK_PATH
    PRELINK_PATH_MASK
    PYTHONPATH
    ROOTPATH )
INTERNAL_CLASS=( COLON_SEPARATED
    LDPATH
    SPACE_SEPARATED )

# Recognized file formats:
MIME_WHITELIST=( text/plain text/x-makefile )

# Configuration files
ENVIRONMENT="${ROOT}/etc/environment"
ENVPROFILE="${ROOT}/etc/profile.env"
LDCONFIG="${ROOT}/etc/ld.so.conf"
PRELINK="${ROOT}/etc/prelink.conf"
LDMTIMEDB="${ROOT}/var/lib/eclectic/env/ld-mtimedb"

# Keep all stored LDPATHS
ECLECTIC_LDPATH=( )

# On systems using non-glibc, ldconfig may not exist, so skip it entirely
# if it does not exist
ldconfig=1
type -fPp ldconfig >/dev/null 2>&1 || ldconfig=0

# is_envd_file()
# Return successfuly when file can be sourced.
is_envfile() {
    local mime envfile=${1}

    # Make sure it is a file and no backup file
    [[ -f ${envfile} ]] || return 1
    [[ -n ${envfile##*~} ]] || return 1
    [[ ${envfile##*.} != bak ]] || return 1

    mime=$(file -i "${envfile}" | cut -d ' ' -f 2 | sed -e 's/;$//')
    if ! has "${mime}" "${MIME_WHITELIST[@]}" ; then
        echo "Skipping non-text file ${envfile}."
        return 1
    fi

    return 0
}

arrayify() {
    local IFS=${1}
    builtin eval "${2}=( \${!2} )"
}

scalarify() {
    local IFS=${1}
    local ref="${2}[*]"
    echo "${!ref}"
}

# update_envvar_classes()
# Update the contents of *_CLASS based on env,d files.
update_envvar_classes() {
    local -a envfiles
    local value
    envfiles=( "${ROOT}"/etc/env.d/* )

    for envfile in "${envfiles[@]}" ; do
        is_envfile "${envfile}" || continue

        value=$(load_config "${envfile}" COLON_SEPARATED)
        arrayify "${IFS}" value
        for x in "${value[@]}" ; do
            has "${x}" "${PATH_CLASS[@]}" && continue
            PATH_CLASS+=( "${x}" )
        done

        value=$(load_config "${envfile}" SPACE_SEPARATED)
        arrayify "${IFS}" value
        for x in "${value[@]}" ; do
            has "${x}" "${SPACE_CLASS[@]}" && continue
            SPACE_CLASS+=( "${x}" )
        done
    done
}

# create_environment_file()
# Create environment file
create_environment_file() {
    local -a envfiles
    local vars store items tmpprofile
    envfiles=( "${ROOT}"/etc/env.d/* )

    # Blank the file first!
    tmpprofile="$(mktemp "${ROOT}"/tmp/profile.XXXXXX)"
    [[ $? = 0 ]] || die "Couldn't create temporary file!"

    # Gather ye classes while ye may!
    update_envvar_classes

    # Parse all files in env.d
    for envfile in "${envfiles[@]}" ; do
        is_envfile "${envfile}" || continue

        # Which vars are to be loaded?
        # TODO: Change to bash magic?
        vars=$(sed \
            -e '/^#/d' -e '/^\s*$/d' -e '/^.*=/s/^\([^=]*\)=.*/\1/' \
            "${envfile}")
        [[ -z ${vars} ]] && continue
        arrayify "${IFS}" vars
        for var in "${vars[@]}" ; do
            # Don't add internal variables to the user environment.
            if has "${var}" "${INTERNAL_CLASS[@]}"; then
                # Store LDPATH for later processing
                if [[ "${var}" == "LDPATH" ]]; then
                    items=$(load_config "${envfile}" LDPATH)
                    arrayify : items
                    for item in "${items[@]}" ; do
                        has "${item}" "${ECLECTIC_LDPATH[@]}" && continue
                        ECLECTIC_LDPATH+=( "${item}" )
                    done
                fi
                continue
            fi
            # Colon separated?...
            if has "${var}" "${PATH_CLASS[@]}" ; then
                store=$(load_config "${tmpprofile}" "${var}")
                if [[ -z ${store} ]] ; then
                    store=$(load_config "${envfile}" ${var})
                    arrayify : store
                else
                    arrayify : store
                    items="$(load_config "${envfile}" ${var})"
                    arrayify : items
                    for item in "${items[@]}" ; do
                        has "${item}" "${store[@]}" && continue
                        store+=( "${item}" )
                    done
                fi
                store_config "${tmpprofile}" "${var}" "$(scalarify : store)"
                continue
            fi
            # Space separated!...
            if has "${var}" "${SPACE_CLASS[@]}" ; then
                store=$(load_config "${tmpprofile}" "${var}")
                if [[ -z ${store} ]] ; then
                    store=$(load_config "${envfile}" "${var}")
                    arrayify "${IFS}" store
                else
                    arrayify "${IFS}" store
                    items=$(load_config "${envfile}" "${var}")
                    arrayify "${IFS}" items
                    for item in "${items[@]}" ; do
                        has "${item}" "${store[@]}" && continue
                        store+=( "${item}" )
                    done
                fi
                store_config "${tmpprofile}" "${var}" "$(scalarify "${IFS}" store)"
                continue
            fi
            # Ok, just a non-cummultative var.
            store_config \
                "${tmpprofile}" \
                "${var}" \
                "$(load_config "${envfile}" "${var}")"
        done
    done

    # Move new file onto old one
    ENVIRONMENT=$(realpath "${ENVIRONMENT}")
    chmod a+r "${tmpprofile}"
    mv "${tmpprofile}" "${ENVIRONMENT}" \
        || die "Couldn't move ${tmpprofile} to ${ENVIRONMENT}!\n
            Original environment file remains unchanged."
}

# create_ld_so_conf()
# Create ld.so.conf file based upon gathered LDPATHs
create_ld_so_conf() {
    [[ -z ${ECLECTIC_LDPATH[@]} ]] && die -q 'No LDPATHs found in ${ROOT}/etc/env.d/*'
    local str="/usr/@TARGET@/lib\n" target new_str
    for x in "${ECLECTIC_LDPATH[@]}" ; do
        str+="${x}\n"
    done

    for target in "${ROOT}"/etc/env.d/targets/*; do
        target=${target#${ROOT}/etc/env.d/targets/}
        [[ "${target}" == '*' ]] && break # no targets installed, just skip this entirely
        new_str=${str//@TARGET@/${target}}
        [[ -f "${ROOT}"/etc/ld-${target}.path ]] || touch "${ROOT}"/etc/ld-${target}.path
        echo -en "${new_str}" > "${ROOT}"/etc/ld-${target}.path
    done

    # for backwards compatibility
    new_str=${str//@TARGET@/host}
    echo -en "${new_str}" > "${LDCONFIG}"
}

# create_prelink_conf()
# Create prelink.conf file based upon existing environment file
create_prelink_conf() {
    [[ -z ${ECLECTIC_LDPATH[@]} ]] && die -q 'No LDPATHs found in ${ROOT}/etc/env.d/*'
    local str
    str="# prelink.conf autogenerated by eclectic\n"
    str+="# Make all changes to /etc/env.d files\n"
    # Add default items
    for x in /bin /sbin /usr/bin /usr/sbin ; do
        str+="-l ${x}\n"
    done
    for x in $(list_libdirs) ; do
        [[ -e ${ROOT}/${x} ]] && str+="-l /${x}\n"
        [[ -e ${ROOT}/usr/${x} ]] && str+="-l /usr/${x}\n"
    done
    prelink_mask=$(load_config "${ENVIRONMENT}" PRELINK_PATH_MASK)
    arrayify : prelink_mask
    path=$(load_config "${ENVIRONMENT}" PATH)
    arrayify : path
    prelink_path=$(load_config "${ENVIRONMENT}" PRELINK_PATH)
    arrayify : prelink_path
    for x in "${path[@]}" "${prelink_path[@]}" "${ECLECTIC_LDPATH[@]}" ; do
        has "${x}" "${prelink_mask[@]}" && continue
        [[ -z ${x##*/} ]] || x+=/
        str+="-h ${x}\n"
    done
    for x in "${prelink_mask[@]}" ; do
        str+="-b ${x}\n"
    done
    echo -e "${str}" > "${PRELINK}"
}

# need_links()
# Returns true if any item of ${LDPATH} has been modified.
need_links() {
    local ret=1
    for x in "${ECLECTIC_LDPATH[@]}" ; do
        [[ -e ${ROOT}${x} ]] || continue
        y=${x//\//_}
        y=${y//-/_}
        y=${y//./_}
        y=${y//+/_}
        oldmtime=$(load_config "${LDMTIMEDB}" "mtime${y}")
        newmtime=$(stat -c %Y "${ROOT}${x}" 2> /dev/null)
        if [[ ${oldmtime} != ${newmtime} ]] ; then
            ret=0
            store_config "${LDMTIMEDB}" "mtime${y}" "${newmtime}"
        fi
    done
    return ${ret}
}

# update_ldcache()
# Update ld.so.cache using ldconfig
update_ldcache() {
    for target in "${ROOT}"/etc/env.d/targets/*; do
        target=${target#${ROOT}/etc/env.d/targets/}
        new_str=${str//@TARGET@/${target}}

        # musl-libc uses only the .path file, but glibc has both .path and
        # .cache files generated for it.
        # however, we should only print whatever is relevant for the libc
        # that is being generated for.

        [[ "${target}" == *-musl* ]] || echo "Regenerating ${ROOT}/etc/ld-${target}.cache..."
        echo "Regenerating ${ROOT}/etc/ld-${target}.path..."
        (
            cd /
            ldconfig ${1} \
                -r "${ROOT:-/}" \
                -f "${ROOT}"/etc/ld-${target}.path \
                -C "${ROOT}"/etc/ld-${target}.cache
        )
    done

    # for backwards compatibility again
    echo "Regenerating ${ROOT}/etc/ld.so.cache..."
    (
        cd /
        ldconfig ${1} -r "${ROOT:-/}"
    )
}

### update action

describe_update() {
    echo "Collect environment variables from all scripts in /etc/env.d/"
}

describe_update_parameters() {
    echo "<makelinks>"
}

describe_update_options() {
    echo "makelinks : Specify \"makelinks\" to force updating of links"
    echo "noldconfig : Do not alter the ld.so cache or configuration."
}

do_update() {
    local makelinks
    while [[ ${#@} -gt 0 ]] ; do
        case ${1} in
            makelinks)
                makelinks="-X"
                ;;
            noldconfig)
                ldconfig=0
                ;;
            *)
                die -q "Unknown option '${1}'"
                ;;
        esac
        shift
    done

    if [[ -e ${ENVIRONMENT} ]] ; then
        [[ -w ${ENVIRONMENT} ]] \
            || die -q "You need to be root!"
    else
        touch "${ENVIRONMENT}" \
            || die -q "You need to be root!"
    fi

    # Create configuration files
    create_environment_file
    if [[ ${ldconfig} == 1 ]] ; then
        create_ld_so_conf
        [[ -e ${ROOT}/usr/sbin/prelink ]] && create_prelink_conf
        need_links && makelinks="-X"
        update_ldcache ${makelinks}
    fi

    # create ${ENVPROFILE}
    cp "${ENVIRONMENT}" "${ENVPROFILE}"
    cp "${ENVIRONMENT}" "${ENVPROFILE/.env/.csh}"
    sed -i \
        -e "s/^\(.*\)=\"\(.*\)\"/export \1='\2'/" \
        "$(realpath "${ENVPROFILE}")"
    sed -i \
        -e "s/^\(.*\)=\"\(.*\)\"/setenv \1 '\2'/" \
        "$(realpath "${ENVPROFILE/.env/.csh}")"
}

# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :
